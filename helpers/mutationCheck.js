const verticalMutations = (dna) => {
    let mutations = 0;
    for(let row = 0; row < dna.length; row++) {
        if (!dna[row + 3]) { // Validamos si existen 3 letras mas si hacemos una linea en forma vertical  
            return mutations; // En caso de no existir termina y retorna las mutaciones encontradas
        }

        const currentRow = dna[row]; // Fila actual

        for (let index = 0; index < currentRow.length; index++) { // Recorremos cada letra de la fila
            let sequence = 1;
            const letter = currentRow[index]; // Letra actual
            
            for (let nextRowIndex = row+1; nextRowIndex < row + 4; nextRowIndex++) { // Revisamos las 3 letras en las siguientes filas 
                const nextLetter = dna[nextRowIndex][index];
                if (nextLetter != letter) { // Si es distinta, termina la secuencia
                    break;
                } else { // Si es la misma letra se suma uno a la secuencia, si la secuencia es 4 se corta el for y se suma a las mutaciones
                    sequence ++;
                    if (sequence === 4) {
                        mutations++;
                        break;
                    }
                }
            }
        }
    };
}

const horizontalMutations = (dna) => {
    let mutations = 0;
    for(let row = 0; row < dna.length; row++) {
        const currentRow = dna[row]; // Fila actual

        for (let index = 0; index < currentRow.length; index++) { // Recorremos cada letra de la fila
            if (!currentRow[index+3]) {
                break;
            }
            let sequence = 1;
            const letter = currentRow[index]; // Letra actual
            for (let nextLetterI = index+1; nextLetterI < index + 4; nextLetterI++) { // Revisamos las 3 letras siguientes en la fila
                const nextLetter = currentRow[nextLetterI];
                if (nextLetter != letter) { // Si es distinta, termina la secuencia
                    break;
                } else { // Si es la misma letra se suma uno a la secuencia, si la secuencia es 4 se corta el for y se suma a las mutaciones
                    sequence ++;
                    if (sequence === 4) {
                        mutations++;
                        break;
                    }
                }
            }
        }
    }
    return mutations;
}

const diagonalMutations = (dna) => {
    let mutations = 0;
    for(let row = 0; row < dna.length; row++) {
        const currentRow = dna[row]; // Fila actual
        for (let index = 0; index < currentRow.length; index++) { // Recorremos cada letra de la fila de izquierda a derecha
            if (!dna[row+3][index+3]) {
                break;
            }
            let sequence = 1;
            const letter = currentRow[index]; // Letra actual
            let NextRow = 1;
            for (let nextLetterI = index+1; nextLetterI < index + 4; nextLetterI++) { // Revisamos las 3 letras siguientes en la fila
                const nextLetter = dna[row + NextRow][nextLetterI];
                NextRow++
                if (nextLetter != letter) { // Si es distinta, termina la secuencia
                    break;
                } else { // Si es la misma letra se suma uno a la secuencia, si la secuencia es 4 se corta el for y se suma a las mutaciones
                    sequence ++;
                    if (sequence === 4) {
                        mutations++;
                        break;
                    }
                }
            }
        }

        for (let index = currentRow.length - 1; index >= 0; index--) { // De derecha a izquierda
            if (!dna[row+3][index-3]) {
                break;
            }

            let sequence = 1;
            const letter = currentRow[index]; // Letra actual
            
            for (let nextLetterI = index-1; nextLetterI > index - 4; nextLetterI--) { // Revisamos las 3 letras siguientes en la fila
                const nextLetter = dna[row + 1][nextLetterI];
                if (nextLetter != letter) { // Si es distinta, termina la secuencia
                    break;
                } else { // Si es la misma letra se suma uno a la secuencia, si la secuencia es 4 se corta el for y se suma a las mutaciones
                    sequence ++;
                    if (sequence = 4) {
                        mutations++;
                        break;
                    }
                }
            }
        }

        return mutations;
    }
}

const getMutations = (dnaStrings) => {
    const DNA2dArray = []
    dnaStrings.forEach((element, index) => { // Transformamos el arreglo de strings a un arreglo bidimensional
        DNA2dArray.push([]);
        for (const letter of element) {
            DNA2dArray[index].push(letter);
        }
    });
    const mutations = verticalMutations(DNA2dArray) + horizontalMutations(DNA2dArray) + diagonalMutations(DNA2dArray)
    return mutations;
}

export default getMutations