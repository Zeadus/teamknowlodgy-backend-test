-- Query para crear Tabla para usar en DB Local
CREATE TABLE mutations(
	id serial primary key,
	DNA VARCHAR not null,
	mutation_count integer
);