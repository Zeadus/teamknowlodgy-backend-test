import express from 'express';
import cors from 'cors';
import getMutations from './helpers/mutationCheck.js';
import PG from 'pg'
const Pool = PG.Pool

const connectionString = process.env.DATABASE_URL; // string de conexion a base de datos de postgre
const pool = new Pool({
  connectionString,
  ssl: {rejectUnauthorized: false}
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

const port = process.env.PORT || 3001;

app.post('/mutation', async (req, res) => {
    if (!req.body.dna || !Array.isArray(req.body.dna) ) { // Validaciones basica de que exista el arreglo dna, se podria usar una libreria de validaciones pero seria innecesario cuando se puede hacer una validacion sencilla
        return res.json({
            status: 403,
            message: 'Invalid DNA'
        }).status(403).end();
    } else {
        const DNARows = req.body.dna.length;
        req.body.dna.forEach(row => {
            if (/[^TAGC]/.test(row)) { // Validamos valores TAGC
                return res.status(403).json({
                    status: 403,
                    message: 'Invalid Nitrogen Base'
                }).end();
            }
            if (row.length != DNARows) { // Validamos longitud de la palabra para asegurar que sea NxN
                return res.status(403).json({
                    status: 403,
                    message: `Nitrogen Base has to be as long as the amount of rows. Rows: ${DNARows}, Length: ${row.length}` 
                }).end();
            }
        });
    }
    const coincidences = getMutations(req.body.dna); 
    const mutations = coincidences > 1 ? 1 : 0; // Si hay mas de una secuencia es mutante
    const query = pool.query('INSERT INTO mutations (DNA, mutation_count) VALUES($1, $2)', [JSON.stringify(req.body.dna), mutations]);
    if (mutations > 0) {
        return res.json({
            status: 200,
            mutations,
            coincidences
        }).status(200).end();
    } else {
        return res.status(403).json({
            status: 403,
            mutations,
            coincidences,
            message: 'No mutations found'
        }).end();
    }

});

app.get('/stats', async (req,res) => {
    const query = await pool.query(`SELECT mut.*, mut.count_mutations::FLOAT/mut.count_no_mutations::FLOAT AS ratio FROM 
    (SELECT
        (SELECT count(*) from mutations where mutation_count > 0) as count_mutations, (SELECT count(*) from mutations where mutation_count = 0) as count_no_mutations
    ) mut`);
    const queryRes = query.rows[0];
    res.json(queryRes)
});

app.listen(port, () => {
    console.log(`Server is running on PORT ${port}`);
});

export default app;
