# Prueba backend Teamknowlodgy

Crear DB de postgreSQL local, crear la tabla con el query que esta en el archivo DB.sql, Correr npm install, setear la variable de entorno para la DB de postgreSQL y npm start para iniciar el proyecto, npm test para correr las pruebas unitarias.

Para la prueba se utilizo:
-Express para declarar las rutas
-pg para hacer llamadas a la base de datos

El codigo esta documentado explicando que hace cada funcion y como funciona la validacion de las mutaciones

Se hizo un deploy en heroku del proyecto, el URL es https://teamknowlodgy-test.herokuapp.com (POST /mutation y GET /stats)