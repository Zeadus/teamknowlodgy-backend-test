import mocha from 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../index.js';

chai.use(chaiHttp);

const { expect } = chai;

mocha.describe('Find Mutations', () => {
  it('find at least 1 mutation', (done) => {
    chai.request(app)
      .post('/mutation/')
      .set('Accept', 'Application/Json')
      .send({dna: ["ATGCGA","CAGTAC","TTAAGT","AGAAGG","CCCCTA","TCACTG"]})
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.mutations).to.be.above(0);
        done();
      });
  });

  it('should not find any mutation', (done) => {
    chai.request(app)
      .post('/mutation/')
      .set('Accept', 'Application/Json')
      .send({dna: ["ATGCGT","CTGTAC","TTAAGT","AGAAGG","CCTCTA","TCACTG"]})
      .end((err, res) => {
        expect(res.status).to.equal(403);
        done();
      });
  });

  it('should return invalid nitrogen base', (done) => {
    chai.request(app)
      .post('/mutation/')
      .set('Accept', 'Application/Json')
      .send({dna: ["ars","CTGTAC","TTAAGT","AGAAGG","CCTCTA","TCACTG"]})
      .end((err, res) => {
        expect(res.status).to.equal(403);
        expect(res.body.message).to.equal('Invalid Nitrogen Base');
        done();
      });
  });

  it('should return invalid nitrogen base length', (done) => {
    chai.request(app)
      .post('/mutation/')
      .set('Accept', 'Application/Json')
      .send({dna: ["ATGCGAA","CTGTAC","TTAAGT","AGAAGG","CCTCTA","TCACTG"]})
      .end((err, res) => {
        expect(res.status).to.equal(403);
        expect(res.body.message).to.contain('Nitrogen Base has to be as long');
        done();
      });
  });

  it('should return mutation stats', (done) => {
    chai.request(app)
      .get('/stats/')
      .set('Accept', 'Application/Json')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });
});
